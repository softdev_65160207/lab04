/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author Snow
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int turnCount = 0;
    private int row, col;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        turnCount++;
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    public boolean checkWin() {
        if (checkRow('O')) {
            return true;
        } else if (checkCol('X')) {
            return true;
        }
        return false;
    }

    private boolean checkRow(char symbol) {

        if ((table[0][0] == symbol && table[0][1] == symbol && table[0][2] == symbol)
                || (table[1][0] == symbol && table[1][1] == symbol && table[1][2] == symbol)
                || (table[2][0] == symbol && table[2][1] == symbol && table[2][2] == symbol)
                || (table[0][0] == symbol && table[1][0] == symbol && table[2][0] == symbol)
                || (table[0][1] == symbol && table[1][1] == symbol && table[2][1] == symbol)
                || (table[0][2] == symbol && table[1][2] == symbol && table[2][2] == symbol)
                || (table[0][0] == symbol && table[1][1] == symbol && table[2][2] == symbol)
                || (table[0][2] == symbol && table[1][1] == symbol && table[2][0] == symbol)) {
            return true;
        }
        return false;
    }
    private boolean checkCol(char symbol) {
    if ((table[0][0] == symbol && table[0][1] == symbol && table[0][2] == symbol)
                || (table[1][0] == symbol && table[1][1] == symbol && table[1][2] == symbol)
                || (table[2][0] == symbol && table[2][1] == symbol && table[2][2] == symbol)
                || (table[0][0] == symbol && table[1][0] == symbol && table[2][0] == symbol)
                || (table[0][1] == symbol && table[1][1] == symbol && table[2][1] == symbol)
                || (table[0][2] == symbol && table[1][2] == symbol && table[2][2] == symbol)
                || (table[0][0] == symbol && table[1][1] == symbol && table[2][2] == symbol)
                || (table[0][2] == symbol && table[1][1] == symbol && table[2][0] == symbol)) {
            return true;
        }
        return false;
    }
    

    boolean checkDraw() {
        if (turnCount == 9) {
            saveDraw();
            return true;
        }
        return false;
    }
    
    private void saveWin(){
        if(player1 == getCurrentPlayer()){
            player1.win();
            player2.lose();
        }else {
            player1.lose();
            player2.win();
        }
    }
    private void saveDraw(){
        player1.draw();
        player2.draw();
    }
}
